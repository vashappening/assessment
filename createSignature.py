import hash
import rsa

def create_signature(file):
    # Hashed is SHA-1 hash of the pdf file
    hashed = hash.hash_file(file).encode()
    (pubkey, privkey) = rsa.newkeys(528)
    # creates a signature using SHA-1 with the private key and hashed
    signature = rsa.sign(hashed,privkey,'SHA-1')
    return signature, pubkey

