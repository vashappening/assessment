import hash
import rsa

def verify_signature(file,signature,public_key):
    # hashed is SHA-1 hash of the pdf file
    hashed = hash.hash_file(file).encode()
    # sign = extracted data from signature file
    sign = file_open(signature)
    # pubkey = loads public key from the public key file
    pubkey = rsa.PublicKey.load_pkcs1(file_open(public_key))
    # rsa.verify returns the hash format used to sign rsa if the signature is valid
    verification = rsa.verify(hashed,sign,pubkey)

def file_open(file):
    file_data = open(file,'rb').read()
    return file_data
